package com.zaahid.challenge5.ui.homepage.fragment

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.zaahid.challenge5.R
import com.zaahid.challenge5.data.local.database.entity.UserEntity
import com.zaahid.challenge5.databinding.FragmentProfileBinding
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import com.zaahid.challenge5.ui.login.LoginPage
import com.zaahid.challenge5.utils.viewModelFactory
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding?=null
    private val binding get() =_binding!!
    private val userViewModel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(requireContext()))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding  = FragmentProfileBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enabledEdit()
        binding.editButton.setOnClickListener {
            cekForm()
        }
        binding.logoutButton.setOnClickListener {
            userViewModel.setUserId(-1)
            userViewModel.setUserKey("")
            val intent = Intent(context, LoginPage::class.java)
            Toast.makeText(context,"Good Bye",Toast.LENGTH_LONG).show()
            startActivity(intent)
        }
        if(!cekUserKey()){
            val intent = Intent(context, LoginPage::class.java)
            Toast.makeText(context,"Good Bye",Toast.LENGTH_LONG).show()
            startActivity(intent)
        }
        binding.changeLanguage.setOnClickListener {
            showChangeLang()
        }
        val switchCompat = binding.switchTheme
//
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
            switchCompat.isChecked = true;

        switchCompat.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (b) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                switchCompat.setChecked(true)
                userViewModel.setSwitch(true)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                switchCompat.setChecked(false)
                userViewModel.setSwitch(false)
            }
        })

    }
    fun cekUserKey() :Boolean{
        return userViewModel.checkIfUserKeyIsExist()
    }
    fun cekForm(){
        if (binding.textinputusername.text.toString().isEmpty().not()){
            if (binding.textinputpassword.text.toString().isEmpty().not()){
                if (binding.inputConfirmPassword.text.toString().isEmpty().not()){
                    if (binding.textinputalamat.text.toString().isEmpty().not()){
                        register()
                    }else Toast.makeText(requireContext(), getString(R.string.empty_address), Toast.LENGTH_SHORT).show()
                }else Toast.makeText(requireContext(),getString(R.string.confirm_pass_empty), Toast.LENGTH_SHORT).show()
            }else Toast.makeText(requireContext(),getString(R.string.password_is_empty), Toast.LENGTH_SHORT).show()
        }else Toast.makeText(requireContext(),getString(R.string.username_is_empty), Toast.LENGTH_SHORT).show()
    }

    fun register(){
        if (cekPasswordIsSame()){
            userViewModel.updateUser(parseFormToEntity())
            Toast.makeText(requireContext(),getString(R.string.successfully_insert_data), Toast.LENGTH_LONG).show()
        }else Toast.makeText(requireContext(),getString(R.string.password_not_same), Toast.LENGTH_LONG).show()
    }

    fun parseFormToEntity() : UserEntity {
        return UserEntity(
            userId = userViewModel.getUserId(),
            username = binding.textinputusername.text.toString().trim(),
            useremail = binding.textinputemail.text.toString().trim(),
            userpassword = binding.textinputpassword.text.toString().trim(),
            alamat = binding.textinputalamat.text.toString().trim(),

        )
    }
    fun cekPasswordIsSame() : Boolean{
        return binding.textinputpassword.text.toString().trim() == binding.inputConfirmPassword.text.toString().trim()
    }
    fun enabledEdit(){
        userViewModel.getUserByUsername(userViewModel.getUserKey())
        userViewModel.detailDataResult.observe(viewLifecycleOwner){
            showEdit(it.payload)
        }
    }

    private fun showEdit(userEntity: UserEntity?) {
        if (userEntity == null){

        }else{
            binding.textinputusername.setText(userEntity.username)
            binding.textinputemail.setText(userEntity.useremail)
            binding.textinputpassword.setText(userEntity.userpassword)
            binding.textinputalamat.setText(userEntity.alamat)
        }
    }
    private fun showChangeLang() {
        val listItem =  arrayOf("english","Indonesia","日本語")
        val mBuilder = AlertDialog.Builder(requireContext())
        mBuilder.setTitle("Choose Language")
        mBuilder.setSingleChoiceItems(listItem,-1){dialog,which->
            when (which) {
                0 -> {
                    setLocate("en")
                    activity?.recreate()
                }
                1 -> {
                    setLocate("id")
                    activity?.recreate()
                }
                2 -> {
                    setLocate("ja")
                    activity?.recreate()
                }
            }
            dialog.dismiss()
        }
        val  mDialog = mBuilder.create()
        mDialog.show()
    }
    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        requireContext().resources.updateConfiguration(config,requireContext().resources.displayMetrics)
        userViewModel.setLang(Lang)
    }


}
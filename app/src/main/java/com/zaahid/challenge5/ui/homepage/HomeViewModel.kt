package com.zaahid.challenge5.ui.homepage

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zaahid.challenge5.BuildConfig
import com.zaahid.challenge5.data.network.service.ApiClient
import com.zaahid.challenge5.model.MovieRespons
import com.zaahid.challenge5.wrapper.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel():ViewModel() {
    val _searchResult = MutableLiveData<Resource<MovieRespons>>()
    val searResult : LiveData<Resource<MovieRespons>> get() = _searchResult

//    fun popularMovie(lang: String = "en",page: Int = 1) {
//        ApiClient.instance.moviePopular(BuildConfig.API_KEY,lang,page)
//            .enqueue(object :Callback<Resource<MovieRespons>>{
//                override fun onResponse(
//                    call: Call<Resource<MovieRespons>>,
//                    response: Response<Resource<MovieRespons>>
//                ) {
//                    _searchResult.postValue(Resource.Loading())
//                    val body = response.body()
//                    val code = response.code()
//                    Log.d("popular movie","$body + $code")
//                    body?.let { _searchResult.postValue(it) }
//                }
//                override fun onFailure(call: Call<Resource<MovieRespons>>, t: Throwable) {
//                    _searchResult.postValue(Resource.ErrorTrow(t))
//                }
//            })
//    }
//    fun searchMovie(key: String,query: String,lang:String = "en",page :Int = 1) {
//        ApiClient.instance.searchMovie(key,query,lang,page)
//            .enqueue(object :Callback<Resource<MovieRespons>>{
//                override fun onResponse(
//                    call: Call<Resource<MovieRespons>>,
//                    response: Response<Resource<MovieRespons>>
//                ) {
//                    _searchResult.postValue(Resource.Loading())
//                    val body = response.body()
//                    val code = response.code()
//                    Log.d("popular movie","$body + $code")
//                    body?.let {
//                        _searchResult.postValue(it)
//                    }
//                }
//                override fun onFailure(call: Call<Resource<MovieRespons>>, t: Throwable) {
//                    _searchResult.postValue(Resource.ErrorTrow(t))
//                }
//            })
//    }
}
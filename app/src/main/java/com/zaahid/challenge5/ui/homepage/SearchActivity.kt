package com.zaahid.challenge5.ui.homepage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zaahid.challenge5.databinding.ActivitySearchBinding
import com.zaahid.challenge5.ui.homepage.fragment.ItemAdapter
import com.zaahid.challenge5.utils.viewModelFactory

class SearchActivity : AppCompatActivity() {
    private val binding: ActivitySearchBinding by lazy {
        ActivitySearchBinding.inflate(layoutInflater)
    }

    private val viewModel: HomeViewModel by viewModelFactory {
        HomeViewModel()
    }

    private val adapter: ItemAdapter = ItemAdapter{}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        initList()
//        observeData()

    }

//    private fun observeData() {
//        viewModel.searchResult.observe(this) {
//            when (it) {
//                is Resource.Loading -> {
//                    binding.pbPost.isVisible = true
//                    binding.rvPost.isVisible = false
//                    binding.tvError.isVisible = false
//                }
//                is Resource.Error -> {
//                    adapter.clearItems()
//                    binding.pbPost.isVisible = false
//                    binding.rvPost.isVisible = false
//                    binding.tvError.isVisible = true
//                    it.exception?.message?.let { er ->
//                        binding.tvError.text = er
//                    }
//                }
//                is Resource.Empty -> {
//                    adapter.clearItems()
//                    binding.pbPost.isVisible = false
//                    binding.rvPost.isVisible = false
//                    binding.tvError.isVisible = true
//                    binding.tvError.text = "empty"
//                }
//                is Resource.Success -> {
//                    it.payload?.result?.let { data -> adapter.setItems(data) }
//                    binding.pbPost.isVisible = false
//                    binding.rvPost.isVisible = true
//                    binding.tvError.isVisible = false
//                }
//                is Resource.ErrorTrow->{
//                    adapter.clearItems()
//                    binding.pbPost.isVisible = false
//                    binding.rvPost.isVisible = false
//                    binding.tvError.isVisible = true
//                    it.exception?.message?.let { er ->
//                        binding.tvError.text = er
//                    }
//                }
//                else -> {}
//            }
//        }
//    }
//
//    private fun initList() {
//        binding.rvPost.apply {
//            layoutManager = LinearLayoutManager(this@SearchActivity)
//            adapter = this@SearchActivity.adapter
//        }
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.menu_search, menu)
//        val search = menu.findItem(R.id.menu_search_bar)
//        val searchView = search.actionView as SearchView
//        searchView.queryHint = "Search"
//        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                query?.let {
//                    viewModel.searchMovie(it,vie)
//                }
//                return false
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//
//                return true
//            }
//        })
//        return super.onCreateOptionsMenu(menu)
//    }

}
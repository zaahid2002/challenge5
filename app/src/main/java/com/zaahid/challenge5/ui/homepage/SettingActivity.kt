package com.zaahid.challenge5.ui.homepage

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import com.zaahid.challenge5.R
import com.zaahid.challenge5.databinding.ActivitySettingBinding
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import java.util.*

class SettingActivity : AppCompatActivity() {
    private val userViewModel: UserViewModel by lazy {
        UserViewModel(ServiceLocator.provideLocalRepository(baseContext))
    }
    private lateinit var binding: ActivitySettingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.changeLanguage.setOnClickListener {
            showChangeLang()
        }
        binding.switchTheme.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                true -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

                false -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

    }
    private fun showChangeLang() {
        val listItem =  arrayOf("english","Indonesia","日本語")
        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle("Choose Language")
        mBuilder.setSingleChoiceItems(listItem,-1){dialog,which->
            when (which) {
                0 -> {
                    setLocate("en")
                    this.recreate()
                }
                1 -> {
                    setLocate("id")
                    this.recreate()
                }
                2 -> {
                    setLocate("ja")
                    this.recreate()
                }
            }
            dialog.dismiss()
        }
        val  mDialog = mBuilder.create()
        mDialog.show()
    }
    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        baseContext.resources.updateConfiguration(config,baseContext.resources.displayMetrics)
        userViewModel.setLang(Lang)
    }
}
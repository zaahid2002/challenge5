package com.zaahid.challenge5.ui.login

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zaahid.challenge5.R
import com.zaahid.challenge5.data.local.database.entity.UserEntity
import com.zaahid.challenge5.databinding.LoginPageBinding
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import com.zaahid.challenge5.ui.homepage.HomePage
import com.zaahid.challenge5.ui.register.FormRegister
import com.zaahid.challenge5.utils.viewModelFactory
import java.util.*

class  LoginPage : AppCompatActivity() {
    private lateinit var binding: LoginPageBinding

    private val viewModel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(this))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toRegister()
        viewModel.getLang()?.let { setLocate(it) }
        cekstatus()

        binding.buttonLogin.setOnClickListener {
            cekForm()
        }
    }
    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        baseContext.resources.updateConfiguration(config,baseContext.resources.displayMetrics)
    }

    override fun onResume() {
        super.onResume()
        cekstatus()
    }

    private fun cekstatus() {
        if(cekUserKey()){
            val intent = Intent(this,HomePage::class.java)
            Toast.makeText(this, getString(R.string.welcome_back)+" "+viewModel.getUserKey(),Toast.LENGTH_LONG).show()
            startActivity(intent)
        }
    }

    fun toRegister(){
        binding.toregister.setOnClickListener {
            val intent = Intent(this@LoginPage, FormRegister::class.java)
            startActivity(intent)
        }
    }
    fun cekUserKey() :Boolean{
        return viewModel.checkIfUserKeyIsExist()
    }
    fun cekForm(){
        if (binding.inputusename.text.toString().isEmpty().not()){
            if (binding.textinputpassword.text.toString().isEmpty().not()){
                viewModel.getUserByUsername(binding.inputusename.text.toString().trim())
                viewModel.detailDataResult.observe(this){
                    login(it.payload)
                }
            }else Toast.makeText(this,getString(R.string.password_is_empty), Toast.LENGTH_SHORT).show()
        }else Toast.makeText(this,getString(R.string.username_is_empty), Toast.LENGTH_SHORT).show()
    }
    fun login(user : UserEntity?){
        if (user == null){
        }else{
            if (user.username.isEmpty().not()){
                viewModel.setUserKey(user.username.toString())
                viewModel.setUserId(user.userId)
                Toast.makeText(this,user.username+getString(R.string.successfully_logged_in), Toast.LENGTH_SHORT).show()
                val intent = Intent(this,HomePage::class.java)
                startActivity(intent)
            }else Toast.makeText(this,getString(R.string.failed_login), Toast.LENGTH_SHORT).show()
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp()
    }
}
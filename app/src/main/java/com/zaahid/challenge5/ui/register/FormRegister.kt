package com.zaahid.challenge5.ui.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zaahid.challenge5.R
import com.zaahid.challenge5.data.local.database.entity.UserEntity
import com.zaahid.challenge5.databinding.ActivityFormRegisterBinding
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import com.zaahid.challenge5.ui.login.LoginPage
import com.zaahid.challenge5.utils.viewModelFactory

class FormRegister : AppCompatActivity() {
    private val binding: ActivityFormRegisterBinding by lazy {
        ActivityFormRegisterBinding.inflate(layoutInflater)
    }
    private val viewmodel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(this))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.button.setOnClickListener { cekForm() }
    }
    fun cekForm(){
        if (binding.textinputusername.text.toString().isEmpty().not()){
            if (binding.textinputpassword.text.toString().isEmpty().not()){
                if (binding.inputConfirmPassword.text.toString().isEmpty().not()){
                    if (binding.textinputalamat.text.toString().isEmpty().not()){
                        register()
                    }else Toast.makeText(this, getString(R.string.empty_address), Toast.LENGTH_SHORT).show()
                }else Toast.makeText(this,getString(R.string.confirm_pass_empty),Toast.LENGTH_SHORT).show()
            }else Toast.makeText(this,getString(R.string.password_is_empty),Toast.LENGTH_SHORT).show()
        }else Toast.makeText(this,getString(R.string.username_is_empty),Toast.LENGTH_SHORT).show()
    }

    fun register(){
            if (cekPasswordIsSame()){
                viewmodel.insertUser(parseFormToEntity())
                Toast.makeText(this,getString(R.string.successfully_insert_data),Toast.LENGTH_LONG).show()
                val intent = Intent(this, LoginPage::class.java)
                startActivity(intent)
            }else Toast.makeText(this,getString(R.string.password_not_same),Toast.LENGTH_LONG).show()

    }
    fun parseFormToEntity() : UserEntity {
        return UserEntity(
            username = binding.textinputusername.text.toString().trim(),
            useremail = binding.textinputemail.text.toString().trim(),
            userpassword = binding.textinputpassword.text.toString().trim(),
            alamat = binding.textinputalamat.text.toString().trim()
        )
    }
    fun cekPasswordIsSame() : Boolean{
        return binding.textinputpassword.text.toString().trim() == binding.inputConfirmPassword.text.toString().trim()
    }
}
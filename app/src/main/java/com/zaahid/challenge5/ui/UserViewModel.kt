package com.zaahid.challenge5.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zaahid.challenge5.data.local.database.entity.UserEntity
import com.zaahid.challenge5.data.repository.LocalRepository
import com.zaahid.challenge5.wrapper.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UserViewModel(private val repository: LocalRepository) : ViewModel() {
    val detailDataResult = MutableLiveData<Resource<UserEntity?>>()
    val insertResult = MutableLiveData<Resource<Number>>()
    val updateResult = MutableLiveData<Resource<Number>>()

    fun insertUser(userEntity: UserEntity){
        viewModelScope.launch {
            insertResult.postValue(Resource.Loading())
            insertResult.postValue(repository.insertUser(userEntity))
        }
    }
    fun getUserByUsername(username : String) {
        viewModelScope.launch {
            detailDataResult.postValue(Resource.Loading())
            detailDataResult.postValue(repository.getUserByUsername(username))
        }
    }
    fun updateUser(userEntity: UserEntity){
        viewModelScope.launch {
            updateResult.postValue(Resource.Loading())
            updateResult.postValue(repository.updateUser(userEntity))
        }
    }
    fun checkIfUserKeyIsExist() : Boolean{
        return repository.checkIfUserKeyIsExist()
    }
    fun setUserKey(user :String){
        repository.setUserKey(user)
    }
    fun getUserKey():String{
        return repository.getUserKey()
    }
    fun setUserId(id : Int){
        repository.setUserId(id)
    }
    fun getUserId():Int{
        return repository.getUserId()
    }
    fun getLang():String?{
        return repository.getLang()
    }
    fun setLang(lang :String){
        repository.setLang(lang)
    }
    fun getSwitch():Boolean{
        return repository.getSwitch()
    }
    fun setSwitch(it:Boolean){
        repository.setSwitch(it)
    }

}
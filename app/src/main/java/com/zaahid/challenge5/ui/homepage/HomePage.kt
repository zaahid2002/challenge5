package com.zaahid.challenge5.ui.homepage

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.zaahid.challenge5.R
import com.zaahid.challenge5.databinding.ActivityHomePageBinding
import com.zaahid.challenge5.model.Hasil
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import com.zaahid.challenge5.ui.homepage.fragment.ItemAdapter
import com.zaahid.challenge5.ui.homepage.fragment.PopularFragment
import com.zaahid.challenge5.ui.homepage.fragment.ProfileFragment
import com.zaahid.challenge5.ui.homepage.fragment.SearchFragment
import com.zaahid.challenge5.utils.viewModelFactory
import java.util.*

class HomePage : AppCompatActivity() {
    lateinit var binding: ActivityHomePageBinding
    private val viewModel: HomeViewModel by viewModelFactory {
        HomeViewModel()
    }
    private val userViewModel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(baseContext))
    }

    private val adapter: ItemAdapter by lazy {
        ItemAdapter{}
    }
    val fragmentManager = supportFragmentManager
    val fragmentTransaction = fragmentManager.beginTransaction()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomePageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.popular_movie-> {
                    fragmentManager.commit {
                        replace<PopularFragment>(R.id.fragment_container)
                    }
                    true
                }
                R.id.search_movie->{

                        fragmentManager.commit {
                            replace<SearchFragment>(R.id.fragment_container)
                        }
//                    val intent = Intent(this,SearchActivity::class.java)
//                    startActivity(intent)
                    true
                }
                R.id.profile->{

                        fragmentManager.commit {
                            replace<ProfileFragment>(R.id.fragment_container)
                        }
                    true
                }
                else->false
            }
        }
        setLocate(userViewModel.getLang().toString())
    }

    private fun setLocate(Lang: String) {
        val locale = Locale(Lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.setLocale(locale)
        baseContext.resources.updateConfiguration(config,baseContext.resources.displayMetrics)
    }
    fun setFragment(title: String,fragment : Fragment){
        supportActionBar?.title = title
        fragmentManager.commit {
            replace(R.id.fragment_container,fragment)
            addToBackStack(null)
        }
    }
}
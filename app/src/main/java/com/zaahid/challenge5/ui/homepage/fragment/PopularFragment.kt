package com.zaahid.challenge5.ui.homepage.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.challenge5.BuildConfig
import com.zaahid.challenge5.R
import com.zaahid.challenge5.data.network.service.ApiClient
import com.zaahid.challenge5.databinding.FragmentPopularBinding
import com.zaahid.challenge5.model.MovieRespons
import com.zaahid.challenge5.servicelocator.ServiceLocator
import com.zaahid.challenge5.ui.UserViewModel
import com.zaahid.challenge5.ui.homepage.HomeViewModel
import com.zaahid.challenge5.utils.viewModelFactory
import com.zaahid.challenge5.wrapper.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple  subclass.
 * Use the  factory method to
 * create an instance of this fragment.
 */
class PopularFragment : Fragment() {
    private var _binding: FragmentPopularBinding?=null
    private val binding get() =_binding!!
    private val userViewModel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(requireContext()))
    }

    private val adapter:ItemAdapter by lazy {
        ItemAdapter{
            val transaction = activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container,ItemFragment(it))
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding  = FragmentPopularBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initList(view)
        observeData()
        val lang = userViewModel.getLang()
        val username = userViewModel.getUserKey()
        val a = getString(R.string.welcome)+" "+ username
        popularMovie(userViewModel.getLang().toString())

        binding.textWelcomeUser.text =  a
    }

    private fun observeData() {
//        viewModel._searchResult.observe(viewLifecycleOwner) {
//            when (it) {
//                is Resource.Loading -> {
//                    binding.pbPopular.isVisible = true
//                    binding.rvPopular.isVisible = false
//                    binding.tvError.isVisible = false
//                }
//                is Resource.Error -> {
//                    adapter.clearItems()
//                    binding.pbPopular.isVisible = false
//                    binding.rvPopular.isVisible = false
//                    binding.tvError.isVisible = true
//                    it.exception?.message?.let { er ->
//                        binding.tvError.text = er
//                    }
//                }
//                is Resource.Empty -> {
//                    adapter.clearItems()
//                    binding.pbPopular.isVisible = false
//                    binding.rvPopular.isVisible = false
//                    binding.tvError.isVisible = true
//                    binding.tvError.text = getString(R.string.empty_resource)
//                }
//                is Resource.Success -> {
//                    it.payload?.results?.let { data -> adapter.submitData(data) }
//                    binding.pbPopular.isVisible = false
//                    binding.rvPopular.isVisible = true
//                    binding.tvError.isVisible = false
//                }
//                is Resource.ErrorTrow -> {
//                    adapter.clearItems()
//                    binding.pbPopular.isVisible = false
//                    binding.rvPopular.isVisible = false
//                    binding.tvError.isVisible = true
//                    it.exception?.message?.let { er ->
//                        binding.tvError.text = er
//                    }
//                }
//                null -> TODO()
//            }
//        }
    }
    private fun initList(view: View) {
        binding.rvPopular.apply {
            layoutManager = LinearLayoutManager(view.context,LinearLayoutManager.VERTICAL,false)
            adapter = this@PopularFragment.adapter
        }
    }
    fun popularMovie(lang: String = "en",page: Int = 1) {
        ApiClient.instance.moviePopular(BuildConfig.API_KEY,lang,page)
            .enqueue(object : Callback<MovieRespons> {
                override fun onResponse(
                    call: Call<MovieRespons>,
                    response: Response<MovieRespons>
                ) {
                    val body = response.body()
                    val code = response.code()
                    Log.d("POPULAR","$body + $code")

                    if (body != null) {
                        adapter.submitData(body.results)
                    }else Toast.makeText(requireContext(), "body null", Toast.LENGTH_SHORT).show()

                }
                override fun onFailure(call: Call<MovieRespons>, t: Throwable) {
                }
            })
    }


}
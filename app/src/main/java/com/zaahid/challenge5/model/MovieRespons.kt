package com.zaahid.challenge5.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class MovieRespons (
    @SerializedName("page")
    val page : Int?,
    @SerializedName("results")
    val results : List<Hasil>,
    @SerializedName("totalPages")
    val totalPages : Int?,
    @SerializedName("totalResult")
    val totalResult : Int?
)
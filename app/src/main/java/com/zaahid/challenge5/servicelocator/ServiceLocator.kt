package com.zaahid.challenge5.servicelocator

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.zaahid.challenge5.data.local.preference.UserPreferenceDataSource
import com.zaahid.challenge5.data.local.preference.UserPreferenceDataSourceImpl
import com.zaahid.challenge5.data.local.AppDataBase
import com.zaahid.challenge5.data.local.database.dao.UserDao
import com.zaahid.challenge5.data.local.database.datasource.UserDataSource
import com.zaahid.challenge5.data.local.database.datasource.UserDataSourceImpl
import com.zaahid.challenge5.data.local.preference.UserPreference

import com.zaahid.challenge5.data.network.service.TMDBApiService
import com.zaahid.challenge5.data.repository.LocalRepository
import com.zaahid.challenge5.data.repository.LocalRepositoryImpl


object ServiceLocator {
    fun provideUserPreference(context: Context): UserPreference {
        return UserPreference(context)
    }

    fun provideUserPreferenceDataSource(context: Context): UserPreferenceDataSource {
        return UserPreferenceDataSourceImpl(provideUserPreference(context))
    }

    fun provideAppDatabase(appContext: Context): AppDataBase {
        return AppDataBase.getInstance(appContext)
    }

    fun provideUserDao(appContext: Context): UserDao {
        return provideAppDatabase(appContext).userDao()
    }

    fun provideUserDataSource(appContext: Context): UserDataSource {
        return UserDataSourceImpl(provideUserDao(appContext))
    }

    fun provideChucker(appContext: Context): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(appContext).build()
    }


//    fun provideDataSource(apiService: TMDBApiService): SearchMovieDataSource {
//        return SearchMovieDataSourceImpl(apiService)
//    }

    fun provideLocalRepository(context: Context): LocalRepository {
        return LocalRepositoryImpl(
            provideUserPreferenceDataSource(context),
            provideUserDataSource(context)
        )
    }
}
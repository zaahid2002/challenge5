package com.zaahid.challenge5.data.network.service

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.zaahid.challenge5.BuildConfig
import com.zaahid.challenge5.model.MovieRespons
import com.zaahid.challenge5.wrapper.Resource
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface TMDBApiService {
    @GET("movie/popular")
    fun moviePopular(
        @Query("api_key")api_key : String ,
        @Query("language")language : String ,
        @Query("page")page : Int
    ):Call<MovieRespons>

    @GET("search/movie")
    fun searchMovie(
        @Query("api_key")api_key: String ,
        @Query("query",encoded = true)query : String,
        @Query("language") language : String  ,
        @Query("page")page : Int ,
        @Query("include_adult")include_adult :Boolean = false
    ):Call<MovieRespons>

}
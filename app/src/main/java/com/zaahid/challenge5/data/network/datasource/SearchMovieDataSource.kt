package com.zaahid.challenge5.data.network.datasource

import com.zaahid.challenge5.data.network.service.TMDBApiService
import com.zaahid.challenge5.model.MovieRespons
import com.zaahid.challenge5.wrapper.Resource
import retrofit2.Call

//interface SearchMovieDataSource {
//    fun searchMovie(key: String,query : String,lang: String,page: Int) : Call<Resource<MovieRespons>>
//    fun moviePopular(lang :String):Call<Resource<MovieRespons>>
//}
//
//class SearchMovieDataSourceImpl(private val api : TMDBApiService):
//    SearchMovieDataSource {
//    override fun searchMovie(key: String,query: String, lang: String, page : Int): Call<Resource<MovieRespons>> {
//        return api.searchMovie(key,query,lang,page)
//    }
//
//
//    override fun moviePopular(lang : String): Call<Resource<MovieRespons>> {
//        return api.moviePopular(lang)
//    }
//
//}
package com.zaahid.challenge5.data.local.preference

interface UserPreferenceDataSource {
    fun getUserKey() : String?
    fun setUserKey(newAppKey:String)
    fun getLang():String?
    fun setLang(lang :String)
    fun getUserId():Int
    fun setUserId(id : Int)
    fun getSwitch():Boolean
    fun setSwitch(it :Boolean)
}
class UserPreferenceDataSourceImpl(
    private val userPreference: UserPreference
) : UserPreferenceDataSource {
    override fun getUserKey(): String? {
        return userPreference.keyuser
    }
    override fun setUserKey(newUserKey: String) {
        userPreference.keyuser = newUserKey
    }

    override fun getLang(): String? {
        return userPreference.langUser
    }

    override fun setLang(lang: String){
        userPreference.langUser = lang
    }

    override fun getUserId(): Int {
        return userPreference.userid
    }

    override fun setUserId(id: Int) {
        userPreference.userid = id
    }

    override fun getSwitch(): Boolean {
        return userPreference.switch
    }

    override fun setSwitch(it: Boolean) {
        userPreference.switch = it
    }
}
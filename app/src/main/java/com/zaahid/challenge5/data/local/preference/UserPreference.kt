package com.zaahid.challenge5.data.local.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class UserPreference(context: Context) {
    private val preference : SharedPreferences = context.getSharedPreferences(NAME, MODE)

    companion object{
        private const val NAME = "username"
        private const val MODE = Context.MODE_PRIVATE
    }
    var keyuser: String?
        get() = preference.getString(
            PreferenceKey.PREF_USER_APP_KEY.first,
            PreferenceKey.PREF_USER_APP_KEY.second
        )
        set(value) = preference.edit {
            this.putString(PreferenceKey.PREF_USER_APP_KEY.first,value)
        }
    var langUser:String?
        get() = preference.getString(
            PreferenceKey.LANG_USER.first,
            PreferenceKey.LANG_USER.second
        )
        set(value) = preference.edit {
            this.putString(PreferenceKey.LANG_USER.first,value)
        }
    var userid:Int
        get() = preference.getInt(
            PreferenceKey.PREF_USER_ID.first,
            PreferenceKey.PREF_USER_ID.second
        )
        set(value) =preference.edit{
            this.putInt(PreferenceKey.PREF_USER_ID.first,value)
        }
    var switch :Boolean
        get() = preference.getBoolean(
            PreferenceKey.PREF_SWITCH.first,
            PreferenceKey.PREF_SWITCH.second
        )
        set(value)= preference.edit{
            this.putBoolean(PreferenceKey.PREF_SWITCH.first,value)
        }
    }


object PreferenceKey{
    val PREF_USER_APP_KEY = Pair("PREF_USER_APP_KEY",null)
    val LANG_USER = Pair("LANG_USER",null)
    val PREF_USER_ID = Pair("PREF_USER_ID",-1)
    val PREF_SWITCH = Pair("PREF_SWITCH",true)

}
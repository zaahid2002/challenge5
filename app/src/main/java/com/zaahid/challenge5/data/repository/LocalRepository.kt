package com.zaahid.challenge5.data.repository

import com.zaahid.challenge5.data.local.preference.UserPreferenceDataSource
import com.zaahid.challenge5.data.local.database.datasource.UserDataSource
import com.zaahid.challenge5.data.local.database.entity.UserEntity
import com.zaahid.challenge5.wrapper.Resource

interface LocalRepository {
    fun checkIfUserKeyIsExist(): Boolean
    fun setUserKey(newAppKey: String)
    fun getUserKey() :String
    fun getLang():String?
    fun setLang(lang :String)
    fun getUserId():Int
    fun setUserId(id:Int)
    fun getSwitch():Boolean
    fun setSwitch(it:Boolean)
    suspend fun getUserByUsername(username : String): Resource<UserEntity?>
    suspend fun insertUser(user : UserEntity): Resource<Number>
    suspend fun updateUser(user: UserEntity):Resource<Number>

}

class LocalRepositoryImpl(
    private val userPreferenceDataSource: UserPreferenceDataSource,
    private val userDataSource: UserDataSource
) : LocalRepository {

    override fun checkIfUserKeyIsExist(): Boolean {
        return userPreferenceDataSource.getUserKey().isNullOrEmpty().not()
    }

    override fun setUserKey(newAppKey: String) {
        userPreferenceDataSource.setUserKey(newAppKey)
    }

    override fun getUserKey(): String {
        return userPreferenceDataSource.getUserKey().toString()
    }

    override fun getLang(): String? {
        return userPreferenceDataSource.getLang()
    }

    override fun setLang(lang: String) {
        userPreferenceDataSource.setLang(lang)
    }

    override fun getUserId(): Int {
        return userPreferenceDataSource.getUserId()
    }

    override fun setUserId(id: Int) {
        userPreferenceDataSource.setUserId(id)
    }

    override fun getSwitch(): Boolean {
        return userPreferenceDataSource.getSwitch()
    }

    override fun setSwitch(it: Boolean) {
        userPreferenceDataSource.setSwitch(it)
    }

    override suspend fun getUserByUsername(username: String): Resource<UserEntity?> {
        return proceed {
            userDataSource.getUserByusername(username)
        }
    }

    override suspend fun insertUser(user: UserEntity): Resource<Number> {
        return proceed {
            userDataSource.insertUser(user)
        }
    }

    override suspend fun updateUser(user: UserEntity): Resource<Number> {
        return proceed {
            userDataSource.updateUser(user)
        }
    }

    private suspend fun <T> proceed(coroutine: suspend () -> T): Resource<T> {
        return try {
            Resource.Success(coroutine.invoke())
        } catch (exception: Exception) {
            Resource.Error(exception)
        }
    }

}
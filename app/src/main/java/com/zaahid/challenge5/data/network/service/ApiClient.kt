package com.zaahid.challenge5.data.network.service

import com.zaahid.challenge5.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private val logging :HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            return httpLoggingInterceptor.apply {
                httpLoggingInterceptor.level =HttpLoggingInterceptor.Level.BODY
            }
        }
    val authInterceptor = Interceptor {
        val originRequest = it.request()
        val oldUrl = originRequest.url
        val newUrl = oldUrl.newBuilder().apply {
            addQueryParameter("api_key", BuildConfig.API_KEY)
        }.build()
        it.proceed(originRequest.newBuilder().url(newUrl).build())
    }

    private val client = OkHttpClient.Builder().addInterceptor(logging).build()

    val instance : TMDBApiService by lazy{
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        retrofit.create(TMDBApiService::class.java)
    }
}